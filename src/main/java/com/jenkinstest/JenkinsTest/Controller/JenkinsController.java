package com.jenkinstest.JenkinsTest.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping("/JenkinsTest")
public class JenkinsController {
	
	@RequestMapping(path = "/test", method = RequestMethod.GET)
	@ResponseBody
	public String checkServiceStatus() {
		return "Success";

	}

}
